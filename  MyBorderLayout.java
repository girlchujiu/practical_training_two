import javax.swing.JButton;
import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
 
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class MyBorderLayout extends JFrame implements ActionListener{
    public static void main(String[] args) {
        MyBorderLayout mb = new MyBorderLayout();
    }
	private JButton jb;
	public MyBorderLayout()
    {
		JFrame frm = new JFrame("Fighting");
        // 创建布局管理器实例border，水平间隔为5，垂直间隔为7,
        BorderLayout border = new BorderLayout(8, 10);
        // 设置frm的页面布局为border
        frm.setLayout(border);
        frm.setSize(280, 200);
        JButton b1 = new JButton("词频统计");
        JButton b2 = new JButton("功能一");
        JButton b3 = new JButton("功能二");
        JButton b4 = new JButton("功能三");
        JButton b5 = new JButton("Welcome");
		
		
        // 将按钮添加到frm的上、下、左、右、中
        frm.add(b1, BorderLayout.NORTH);
        frm.add(b5, BorderLayout.SOUTH);
        frm.add(b2, BorderLayout.WEST);
        frm.add(b4, BorderLayout.EAST);
        frm.add(b3, BorderLayout.CENTER);
		//b2.setActionCommand();
		//this.add(frm);
		b2.addActionListener(this);//加入事件监听
        frm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frm.setVisible(true);
        //this.setSize(300, 200);
        //this.setLocation(300, 400);
        //jb=new JButton("跳转");
        //this.add(b2);
        //jb.addActionListener(this);//加入事件监听
        //this.setVisible(true);
        
    }
    //public static void main(String[] args) {
    //    // TODO Auto-generated method stub
    //       Frm frame=new Frm();
    //       
    //}
   
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
            this.dispose();
            new Frm();
    }
}
