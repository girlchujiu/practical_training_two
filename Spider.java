import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
 
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
 
public class Spider extends JFrame
{
    private static final long serialVersionUID = 1L;
 
    public Spider()
    {
        setTitle("text");
        setSize(500, 190);
        setResizable(false);
        setLayout(new BorderLayout());
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
 
    private Spider addComponents()
    {
        final JTextArea area = new JTextArea();
        JScrollPane pane = new JScrollPane(area);
        add(pane, BorderLayout.CENTER);
        JPanel bottom = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JButton btn1 = new JButton("auto");
        btn1.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                area.setLineWrap(true);
                area.setWrapStyleWord(true);
            }
        });
        JButton btn2 = new JButton("unauto");
        btn2.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                area.setWrapStyleWord(false);
                area.setLineWrap(false);
            }
        });
        bottom.add(btn1);
        bottom.add(btn2);
        add(bottom, BorderLayout.SOUTH);
        return this;
    }
 
    public static void main(String[] args)
    {
        new Spider().addComponents().setVisible(true);
    }
}